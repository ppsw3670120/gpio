#include <LPC21xx.H>

#define LED0_bm 1<<16
#define LED1_bm 1<<17
#define LED2_bm 1<<18
#define LED3_bm 1<<19

#define Button0_bm 1<<4
#define Button1_bm 1<<6
#define Button2_bm 1<<5
#define Button3_bm 1<<7

void Delay(unsigned int uiTime)
{
	unsigned int uiForLoopCounter;
	
	for(uiForLoopCounter=0;uiForLoopCounter<uiTime*5460;uiForLoopCounter++) {}
}

void LedInit() {
	IO1DIR = (LED0_bm|LED1_bm|LED2_bm|LED3_bm); // ustawia piny 16-19 portu 1 jako wyjsciowe
	IO1SET = LED0_bm;
}

void LedOn(unsigned char ucLedIndeks) {
	IO1CLR=LED0_bm|LED1_bm|LED2_bm|LED3_bm;
	switch(ucLedIndeks) {
		case 0:
			IO1SET=LED0_bm;
			break;
		case 1:
			IO1SET=LED1_bm;
			break;
		case 2:
			IO1SET=LED2_bm;
			break;
		case 3:
			IO1SET=LED3_bm;
			break;
		default: {}
		}
}

enum KeyboardState {RELEASED,BUTTON_0,BUTTON_1,BUTTON_2,BUTTON_3};
enum KeyboardState eKeyboardRead() {
	if((IO0PIN&Button0_bm)==0) {
		return BUTTON_0;
	}
	else if((IO0PIN&Button1_bm)==0) { //wcisniety: 0x0  puszczony: 0x40
		return BUTTON_1;
	}
	else if((IO0PIN&Button2_bm)==0) { //wcisniety: 0x0 puszczony: 0x20
		return BUTTON_2;
	}
	else if((IO0PIN&Button3_bm)==0) {
		return BUTTON_3;
	}
	return RELEASED;
}

void KeyboardInit() {
	IO0DIR=IO0DIR&(~(Button0_bm|Button1_bm|Button2_bm|Button3_bm)); //ustawia piny 4-7 portu 0 jako wejsciowe
}

enum StepDirection {LEFT,RIGHT};

void LedStep (enum StepDirection Kierunek){
	static unsigned int uiLedIndeks;
	if(Kierunek==RIGHT) {
		uiLedIndeks--;
	}
	else if (Kierunek==LEFT){
		uiLedIndeks++;
	}
	LedOn(uiLedIndeks%4);
}
// gittest
void LedStepLeft(void) {
	LedStep(LEFT);
}
void LedStepRight(void) {
	LedStep(RIGHT);
}

int main() {
	LedInit();
	KeyboardInit();
	while(1) {
		Delay(250);
		switch (eKeyboardRead()) {
			case BUTTON_1:
				LedStepRight();
				break;
			case BUTTON_2:
				LedStepLeft();
				break;
			default: {}
		}
	}
}
